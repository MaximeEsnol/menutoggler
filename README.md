# MenuToggler
## A simple, easy to use JavaScript menu utility 

### What does MenuToggler do?

MenuToggler helps you write less (duplicate) code for simple and common 
JavaScript problems such as having menu's and other elements open and close,
using animations. 

### Installation

1. Download `menutoggler.js` and save it in the `scripts` directory of your project.
2. Load `menutoggler.js` on the pages you want to use it using
```
<script src="path/to/scripts/menutoggler.js"></script>
```

### How to use

MenuToggler works on *any* element that you wish to open and close. It allows for 
functions that are ran while your menu opens and closes. Typically, you would 
assign your animations to the menu in these functions. 

#### 1. Need to know
In MenuToggler, we speak of two different elements. The 'menu' and the 'toggler'.
The 'menu' represents any element you wish to open or close, this can be specifically a
menu, but it can also be anything else you want.
The 'toggler' is the element that is responsible for opening and closing the 'menu'. 
You can think of it as an 'on/off' switch. 

#### 2. Set up Menu and Toggler.

To begin, you have to set up a Menu element and a Toggler element. You don't have 
to include any logic onto either of those, that's what MenuToggler is for. 

```html
<!-- 
    This is your toggler, every toggler needs a 'data-status' attribute, most of the time
    you'll set this to 'closed' on default and let MenuToggler handle it from here.
-->
<div class="side-menu-toggler" data-status="closed">
    <p>Menu</p>
</div>
<!-- 
    This is your actual menu, don't forget to 
    put display: none; in your stylesheet if you want it to be hidden 
    by default. MenuToggler will handle it from here.
-->
<div class="side-menu">
    <p>Boo!</p>
</div>
```

Now that you have a menu and a toggler element, you can proceed to the next step - initializing MenuToggler.

#### 3. Initializing MenuToggler

To be able to open and close the menu, MenuToggler needs to know 
which element is the toggler and which one is the menu that the toggle toggles. (try saying that three times in a row.)

To initialize MenuToggler, you call its constructor `new MenuToggler()`. This 
constructor takes 1 parameter. An associative array that contains all the 
required things that tell MenuToggler how to handle your menu.

```js
let sideMenuToggler = new MenuToggler({
    toggler:        document.querySelector(".side-menu-toggler"),
    menu:           document.querySelector(".side-menu")
});
```

**toggler** required (HTMLElement)
An HTML Element that represents the toggler. You can use `document.querySelector(".your-toggler")` to 
get the element.

**menu** required (HTMLElement)
An HTML Element that represents the menu. You can use `document.querySelector(".your-menu")` to 
get the element.

**display** optional (String)
The CSS "display" property's value that MenuToggler has to assign to this element when its fully opened. 

*Default: "inline-block"*

**timeout** optional (Number)
The timeout in milliseconds before the menu is fully opened or closed. If you have an 
animation for this menu that lasts 1 second, the timeout would be 1000. 

*Default: 0*

**open_action** optional (Function)
The function to run while the menu is opening, before it is fully opened. 
Typically, you would set the animation for the menu in this function.
This function has the 'menu' element as parameter.

*Default: null*

**close_action** optional (Function)
The function to run while the menu is closing, before it is fully closed.
Typically, you would set the closing animation for the menu in this function.
This function has the 'menu' element as parameter.

*Default: null*

**close_on_blur** optional (boolean)
Whether the menu should be closed when the user clicks outside the menu.

*Default: false*

**close_on_scroll** optional (boolean)
Whether the menu should be closed when the user scrolls on the page.

*Default: false*

**debug** optional (boolean)
Whether to print simple debugging messages in the browser console.

*Default: false*

#### 4. Using open_action and close_action

The 'open_action' and 'close_action' are very powerful and allow for a lot 
of creativity. But to use them in the best way possible, it's necessary to know
how they work. 

You can define an open_action or close_action like this: 

```js
const menuToggler = new MenuToggler({
    ...
    open_action:    (menu) => my_open_action(menu),
    close_action:   (menu) => my_close_action(menu)
    ...
});
```

Now you can do anything you want in your `my_open_action` and `my_close_action` functions. 

**Setting an animation when opening the menu**

```js
function my_open_action(menu) {
    menu.style.animation =          "openingMenu .5s ease-in-out";
    menu.style.animationFillMode =  "forwards";
}
``` 

That is all! When you click the toggler, MenuToggler will run `my_open_action()` 
and set the 'openingMenu' animation to the menu. 

To avoid flickering after the animation is completed, set `animationFillMode` to `forwards`. 

Do the same to set a closing animation. 
