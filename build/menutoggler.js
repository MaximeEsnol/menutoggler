"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MenuTogglerStorage = function MenuTogglerStorage() {
  _classCallCheck(this, MenuTogglerStorage);

  console.warn("You initialized MenuTogglerStorage by its constructor. This is however not necessary.");
};

MenuTogglerStorage.instances = [];

MenuTogglerStorage.save = function (hash, instance) {
  MenuTogglerStorage.instances[hash] = instance;
};

MenuTogglerStorage.get = function (hash) {
  return MenuTogglerStorage.instances[hash];
};
/**
 * Manages the opening and closing of the specified navigation menu - or any element that is provided for that matter.
 * To be supported, a toggler element with a data-status attribute has to be provided and the element 
 * that is supposed to open or close upon clicking the toggler. 
 * 
 * The display style can also be passed as an argument and will decide how the element must be displayed when it 
 * is considered "opened". By default this is "inline-block", but it can be any valid CSS 'display' property, except "none".
 * 
 * Additionally, an executable can be provided that will be executed when the element is opening or closing. This 
 * is useful if you want to animate the opening and closing of the element. 
 * By default these are null and nothing special happens. The element just goes from display: none; to display: {displayStyle}
 * 
 * In the doWhileOpening and doWhileClosing functions, you can call the 'darkenPage(isDark)' function to 
 * make the page background lighter or darker when the menu is opened or closed.
 * 
 * @param {JSON} params A JSON array with as key the name of the parameter and value the value of the parameter. 
 * Two required parameters are the toggler element and the menu element. Below is a list of all available parameters.
 * 
 * @param {HTMLElement} toggler (required) The element that toggles the opening and closing. This element must have a 
 * 'data-status' attribute that is set to either "opened" or "closed". 
 * A click event listener will be added to this element automatically.
 * @param {HTMLElement} menu (required) The element that will be opened or closed depending on the 'data-status' 
 * attribute on the toggler element.
 * @param {String} display (optional) How the element has to be displayed after it has been opened. This is by
 * default "inline-block" but it can be any CSS 'display' property except for "none". 
 * @param {int} timeout (optional) How long (in milliseconds) the opening or closing function lasts before the menu has to actually be closed.
 * You could use this when you are using an animation for opening and closing the element. Default value is 0.
 * @param {Function} open_action (optional) A function that will be called before the menu is opened. 
 * @param {Function} close_action (optional) A function that will be called before the menu is closed.
 * @param {boolean} close_on_blur (optional) Whether or not the menu has to be closed when it is no longer the subject of focus.
 * @param {boolean} close_on_scroll (optional) Whether or not the menu has to be closed when scrolling.
 * @param {boolean} debug (optional) Whether or not to enable simple debugging messages when menu actions are ran.
 */


var MenuToggler = function MenuToggler(params) {
  var _this = this;

  _classCallCheck(this, MenuToggler);

  this.NAV_OPEN = "opened";
  this.NAV_CLOSED = "closed";

  this._init = function () {
    var self = _this;

    _this._saveInstance();

    _this.toggler.addEventListener("click", function () {
      self._toggleClickHandler();
    });
  };

  this._saveInstance = function () {
    MenuTogglerStorage.save(MenuToggler.serial, _this);

    _this.menuElement.setAttribute("data-menutoggler-hash", MenuToggler.serial);
  };

  this._toggleClickHandler = function () {
    var status = _this.toggler.getAttribute("data-status");

    if (status == _this.NAV_OPEN) {
      _this.debugMsg("Found that the menu was opened and needs to be closed.");

      _this.closeMenu();
    } else if (status == _this.NAV_CLOSED) {
      _this.debugMsg("Found that the menu was closed and needs to be opened.");

      _this.openMenu();
    } else if (status == undefined) {}
  };

  this.openMenu = function () {
    if (_this.doWhileOpening != null) {
      _this.debugMsg("Calling " + _this.doWhileOpening + ".");

      _this.doWhileOpening.call(_this, _this.menuElement);
    }

    _this.menuElement.style.display = _this.displayStyle;
    setTimeout(function () {
      _this._completeOpening();
    }, _this.timeout);
  };

  this._completeOpening = function () {
    _this.debugMsg("Finishing the opening of the element.");

    _this.toggler.setAttribute("data-status", _this.NAV_OPEN);

    if (_this.allowCloseOnBlur) {
      _this._addCloseOnBlurEventListener();
    }

    if (_this.allowCloseOnScroll) {
      _this._addCloseOnScrollEventListener();
    }
  };

  this.closeMenu = function () {
    if (_this.doWhileClosing != null) {
      _this.debugMsg("Calling: " + _this.doWhileClosing + ".");

      _this.doWhileClosing.call(_this, _this.menuElement);
    }

    setTimeout(function () {
      _this.menuElement.style.display = "none";

      _this._completeClosing();
    }, _this.timeout);
  };

  this._completeClosing = function () {
    _this.debugMsg("Finishing the closing of the element.");

    _this.toggler.setAttribute("data-status", _this.NAV_CLOSED);

    if (_this.allowCloseOnBlur) {
      _this._removeCloseOnBlurEventListener();
    }

    if (_this.allowCloseOnScroll) {
      _this._removeCloseOnScrollEventListener();
    }
  };

  this._addCloseOnScrollEventListener = function () {
    window.addEventListener("scroll", _this._bodyScrollHandler = function (event) {
      _this._scrollEvent(event);
    });
  };

  this._removeCloseOnScrollEventListener = function () {
    window.removeEventListener("scroll", _this._bodyScrollHandler);
  };

  this._addCloseOnBlurEventListener = function () {
    var body = _this.getBody();

    body.addEventListener("click", _this._bodyClickHandler = function (event) {
      _this._blurClickedEvent(event);
    });
  };

  this._removeCloseOnBlurEventListener = function () {
    _this.debugMsg("Close On Blur was enabled, removing the event listener possibly set to 'body'.");

    var body = _this.getBody();

    body.removeEventListener("click", _this._bodyClickHandler);
  };

  this._scrollEvent = function () {
    _this.closeMenu();
  };

  this._blurClickedEvent = function (event) {
    _this.debugMsg("Body was clicked while a Close On Blur event listener is active.");

    var path = event.path || event.composedPath();

    if (!path.includes(_this.menuElement) && !path.includes(_this.toggler)) {
      _this.debugMsg("CloseOnBlur click happened outside of the toggler element or the menu element. Proceeding to close element.");

      _this.closeMenu();
    }
  };

  this.getBody = function () {
    return document.querySelector("body");
  };

  this.debugMsg = function (msg) {
    if (_this.debugMode) {
      console.log("[MENU]: " + msg);
    }
  };

  this.toggler = params.toggler;
  this.menuElement = params.menu;
  this.displayStyle = params.display == undefined ? "inline-block" : params.display;
  this.timeout = params.timeout == undefined ? 0 : params.timeout;
  this.doWhileOpening = params.open_action == undefined ? null : params.open_action;
  this.doWhileClosing = params.close_action == undefined ? null : params.close_action;
  this.allowCloseOnBlur = params.close_on_blur == undefined ? false : params.close_on_blur;
  this.allowCloseOnScroll = params.close_on_scroll == undefined ? false : params.close_on_scroll;
  this.debugMode = params.debug == undefined ? false : params.debug;
  this._bodyClickHandler;
  this._bodyScrollHandler;
  this.debugMsg("Initializing nav menu with toggle element: " + this.toggler + " and menu: " + this.menuElement);

  this._init();

  MenuToggler.serial++;
};

MenuToggler.serial = 1;

MenuToggler.darkenPage = function () {
  var isDark = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var darkElem = document.querySelector("#pl-page-darken-bg");

  if (darkElem == undefined) {
    darkElem = createElement("div", "pl-page-darken-bg");
    document.querySelector("body").appendChild(darkElem);
  }

  if (isDark) {
    darkElem.style.display = "inline-block";
    darkElem.style.animation = "lightenBackground .25s ease-in-out";
    darkElem.style.animationFillMode = null;
    setTimeout(function () {
      return darkElem.style.display = "none";
    }, 250);
  } else {
    darkElem.style.display = "inline-block";
    darkElem.style.animation = "darkenBackground .25s ease-in-out";
    darkElem.style.animationFillMode = "forwards";
  }
};