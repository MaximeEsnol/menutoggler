class MenuTogglerStorage {
    constructor() {
        console.warn("You initialized MenuTogglerStorage by its constructor. This is however not necessary.");
    }
}
MenuTogglerStorage.instances = [];

MenuTogglerStorage.save = (hash, instance) => {
    MenuTogglerStorage.instances[hash] = instance;
}

MenuTogglerStorage.get = (hash) => {
    return MenuTogglerStorage.instances[hash];
}

/**
 * Manages the opening and closing of the specified navigation menu - or any element that is provided for that matter.
 * To be supported, a toggler element with a data-status attribute has to be provided and the element 
 * that is supposed to open or close upon clicking the toggler. 
 * 
 * The display style can also be passed as an argument and will decide how the element must be displayed when it 
 * is considered "opened". By default this is "inline-block", but it can be any valid CSS 'display' property, except "none".
 * 
 * Additionally, an executable can be provided that will be executed when the element is opening or closing. This 
 * is useful if you want to animate the opening and closing of the element. 
 * By default these are null and nothing special happens. The element just goes from display: none; to display: {displayStyle}
 * 
 * In the doWhileOpening and doWhileClosing functions, you can call the 'darkenPage(isDark)' function to 
 * make the page background lighter or darker when the menu is opened or closed.
 * 
 * @param {JSON} params A JSON array with as key the name of the parameter and value the value of the parameter. 
 * Two required parameters are the toggler element and the menu element. Below is a list of all available parameters.
 * 
 * @param {HTMLElement} toggler (required) The element that toggles the opening and closing. This element must have a 
 * 'data-status' attribute that is set to either "opened" or "closed". 
 * A click event listener will be added to this element automatically.
 * @param {HTMLElement} menu (required) The element that will be opened or closed depending on the 'data-status' 
 * attribute on the toggler element.
 * @param {String} display (optional) How the element has to be displayed after it has been opened. This is by
 * default "inline-block" but it can be any CSS 'display' property except for "none". 
 * @param {int} timeout (optional) How long (in milliseconds) the opening or closing function lasts before the menu has to actually be closed.
 * You could use this when you are using an animation for opening and closing the element. Default value is 0.
 * @param {Function} open_action (optional) A function that will be called before the menu is opened. 
 * @param {Function} close_action (optional) A function that will be called before the menu is closed.
 * @param {boolean} close_on_blur (optional) Whether or not the menu has to be closed when it is no longer the subject of focus.
 * @param {boolean} close_on_scroll (optional) Whether or not the menu has to be closed when scrolling.
 * @param {boolean} debug (optional) Whether or not to enable simple debugging messages when menu actions are ran.
 */
class MenuToggler {

    NAV_OPEN = "opened";
    NAV_CLOSED = "closed";

    constructor(params) {
        this.toggler = params.toggler;
        this.menuElement = params.menu;
        this.displayStyle = (params.display == undefined) ? "inline-block" : params.display;
        this.timeout = (params.timeout == undefined) ? 0 : params.timeout;
        this.doWhileOpening = (params.open_action == undefined) ? null : params.open_action;
        this.doWhileClosing = (params.close_action == undefined) ? null : params.close_action;
        this.allowCloseOnBlur = (params.close_on_blur == undefined) ? false : params.close_on_blur;
        this.allowCloseOnScroll = (params.close_on_scroll == undefined) ? false : params.close_on_scroll;
        this.debugMode = (params.debug == undefined) ? false : params.debug;
        this._bodyClickHandler;
        this._bodyScrollHandler;

        this.debugMsg("Initializing nav menu with toggle element: " + this.toggler + " and menu: " + this.menuElement);
        this._init();
        MenuToggler.serial++;
    }

    _init = () => {
        let self = this;

        this._saveInstance();

        this.toggler.addEventListener("click", () => {
            self._toggleClickHandler();
        });
    }

    _saveInstance = () => {
        MenuTogglerStorage.save(MenuToggler.serial, this);
        this.menuElement.setAttribute("data-menutoggler-hash", MenuToggler.serial);
    }

    _toggleClickHandler = () => {
        let status = this.toggler.getAttribute("data-status");

        if (status == this.NAV_OPEN) {
            this.debugMsg("Found that the menu was opened and needs to be closed.");
            this.closeMenu();
        } else if (status == this.NAV_CLOSED) {
            this.debugMsg("Found that the menu was closed and needs to be opened.");
            this.openMenu();
        } else if (status == undefined) {

        }
    }

    openMenu = () => {
        if (this.doWhileOpening != null) {
            this.debugMsg("Calling " + this.doWhileOpening + ".");
            this.doWhileOpening.call(this, this.menuElement);
        }

        this.menuElement.style.display = this.displayStyle;

        setTimeout(() => {
            this._completeOpening();
        }, this.timeout);
    }

    _completeOpening = () => {
        this.debugMsg("Finishing the opening of the element.");
        this.toggler.setAttribute("data-status", this.NAV_OPEN);

        if (this.allowCloseOnBlur) {
            this._addCloseOnBlurEventListener();
        }

        if (this.allowCloseOnScroll) {
            this._addCloseOnScrollEventListener();
        }
    }

    closeMenu = () => {
        if (this.doWhileClosing != null) {
            this.debugMsg("Calling: " + this.doWhileClosing + ".");
            this.doWhileClosing.call(this, this.menuElement);
        }

        setTimeout(() => {
            this.menuElement.style.display = "none";

            this._completeClosing();
        }, this.timeout);
    }

    _completeClosing = () => {
        this.debugMsg("Finishing the closing of the element.");
        this.toggler.setAttribute("data-status", this.NAV_CLOSED);

        if (this.allowCloseOnBlur) {
            this._removeCloseOnBlurEventListener();
        }

        if (this.allowCloseOnScroll) {
            this._removeCloseOnScrollEventListener();
        }
    }

    _addCloseOnScrollEventListener = () => {
        window.addEventListener("scroll", this._bodyScrollHandler = (event) => {
            this._scrollEvent(event);
        });
    }

    _removeCloseOnScrollEventListener = () => {
        window.removeEventListener("scroll", this._bodyScrollHandler);
    }

    _addCloseOnBlurEventListener = () => {
        let body = this.getBody();

        body.addEventListener("click", this._bodyClickHandler = (event) => {
            this._blurClickedEvent(event);
        });
    }

    _removeCloseOnBlurEventListener = () => {
        this.debugMsg("Close On Blur was enabled, removing the event listener possibly set to 'body'.");
        let body = this.getBody();

        body.removeEventListener("click", this._bodyClickHandler);
    }

    _scrollEvent = () => {
        this.closeMenu();
    }

    _blurClickedEvent = (event) => {
        this.debugMsg("Body was clicked while a Close On Blur event listener is active.");
        let path = event.path || event.composedPath();
        if (!path.includes(this.menuElement) && !path.includes(this.toggler)) {
            this.debugMsg("CloseOnBlur click happened outside of the toggler element or the menu element. Proceeding to close element.");
            this.closeMenu();
        }
    }

    getBody = () => {
        return document.querySelector("body");
    }

    debugMsg = (msg) => {
        if (this.debugMode) {
            console.log("[MENU]: " + msg);
        }
    }
}

MenuToggler.serial = 1;

MenuToggler.darkenPage = (isDark = false) => {
    let darkElem = document.querySelector("#pl-page-darken-bg");

    if (darkElem == undefined) {
        darkElem = createElement("div", "pl-page-darken-bg");
        document.querySelector("body").appendChild(darkElem);
    }

    if (isDark) {
        darkElem.style.display = "inline-block";
        darkElem.style.animation = "lightenBackground .25s ease-in-out";
        darkElem.style.animationFillMode = null;
        setTimeout(() => darkElem.style.display = "none", 250);
    } else {
        darkElem.style.display = "inline-block";
        darkElem.style.animation = "darkenBackground .25s ease-in-out";
        darkElem.style.animationFillMode = "forwards";
    }
}